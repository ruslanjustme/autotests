﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using worldisMeTests.Func;

namespace worldisMeTests
{
    public class Manager
    {
        private CreatePostFunctions post;
        private LoginFunctions login;
        private NavigationFunctions navigator;
        protected IWebDriver driver;
        private string baseURL;
        private static ThreadLocal<Manager> man = new ThreadLocal<Manager>();

        private Manager()
        {
            baseURL = Settings.BaseURL;
            driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            driver.Manage().Window.Maximize();
            post = new CreatePostFunctions(this);
            navigator = new NavigationFunctions(this, baseURL);
            login = new LoginFunctions(this);

        }

        public static Manager GetInstance()
        {
            if(!man.IsValueCreated)
            {
                Manager newInstance = new Manager();
                newInstance.Navigator.OpenHomePage();
                man.Value = newInstance;
            }
            return man.Value;
        }

        ~Manager()
        {
            try
            {
                driver.Quit();
            }
            catch(Exception)
            {
                //ignore
            }
        }

        public IWebDriver Driver
        {
            get
            {
                return driver;
            }
        }

        public void Stop()
        {
            driver.Quit();
        }

        public LoginFunctions Login
        {
            get
            {
                return login;
            }
        }

        public CreatePostFunctions Post
        {
            get
            {
                return post;
            }
        }

        public NavigationFunctions Navigator
        {
            get
            {
                return navigator;
            }
        }
    }
}

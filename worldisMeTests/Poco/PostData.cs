﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace worldisMeTests.Poco
{
    public class PostData
    {
        public string Text { get; set; }
        public string Title { get; set; }
        public PostData(string Title, string Text)
        {
            this.Title = Title;
            this.Text = Text;
        }
        public PostData()
        {
        }
    }
}

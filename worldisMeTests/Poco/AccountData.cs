﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace worldisMeTests.Poco
{
    public class AccountData
    {        
        public string Email { get; set; }

        public string Password { get; set; }

        public AccountData(string Email, string Password)
        {
            this.Email = Email;
            this.Password = Password;
        }
    }
}

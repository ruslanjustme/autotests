﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using worldisMeTests.Poco;

namespace worldisMeTests.Func
{
    public class CreatePostFunctions : CommonFunctions
    {
        public CreatePostFunctions(Manager manager) : base(manager)
        {
        }

        public void CreatePost(PostData post)
        {
            driver.FindElement(By.Name("text")).Clear();
            driver.FindElement(By.Name("text")).SendKeys(post.Title + "\n" + post.Text);
            driver.FindElement(By.XPath("//form[@id='Opinion-Post']/div[5]/span[2]/a/span")).Click();
            Thread.Sleep(3000);
        }

        public string GetLastPostTitle()
        {
            string t = driver.FindElements(
                By.CssSelector("div.Action-Item.act-35.aItem-opinion span.Opinion-Text")
                )[0].Text;
            return t;
        }
    }
}

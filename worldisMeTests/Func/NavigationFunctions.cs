﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;


namespace worldisMeTests.Func
{
    public class NavigationFunctions :CommonFunctions
    {
        private string baseURL;

        public NavigationFunctions(Manager manager, string baseURL) : base(manager)
        {
            this.baseURL = baseURL;
        }

        public void OpenLoginPage()
        {
            driver.Navigate().GoToUrl(baseURL + "/");
            Thread.Sleep(1000);
        }

        public void OpenBlogPage()
        {
            driver.Navigate().GoToUrl(baseURL + "/myfeed");
        }

        public void OpenHomePage()
        {
            driver.Navigate().GoToUrl(baseURL);
            Thread.Sleep(2000);
        }
    }
}

﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using worldisMeTests.Poco;

namespace worldisMeTests.Func
{
    public class LoginFunctions :CommonFunctions
    {
        public LoginFunctions(Manager manager) : base(manager)
        {
        }

        public void Login(AccountData user)
        {
            if (LoggedIn())
            {
                return;
            }
            driver.FindElement(By.Id("email")).Clear();
            driver.FindElement(By.Id("email")).SendKeys(user.Email);
            driver.FindElement(By.Id("password")).Clear();
            driver.FindElement(By.Id("password")).SendKeys(user.Password);
            driver.FindElement(By.CssSelector("input.Button-SubmitForm")).Click();
            Thread.Sleep(1000);
        }

        public string GetCurrentUserName()
        {
            return driver.FindElement(By.CssSelector("a[title='Перейти в блог']")).Text;
        }

        public void Logout()
        {
            driver.FindElement(By.CssSelector("#main_user_menu_opener > a.hint_popup.exclude_scroll")).Click();
            driver.FindElement(By.CssSelector("a.npjax > span.p15")).Click();
            Thread.Sleep(1000);
        }

        public bool LoggedIn()
        {
            try
            {
                var justAnId = driver.FindElement(By.CssSelector("#main_user_menu_opener"));
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool AuthError()
        {
            return this.IsElementVisible(By.CssSelector("div.ErrorMessage.hidden"));
        }
    }

}

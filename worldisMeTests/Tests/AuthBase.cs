﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using worldisMeTests.Poco;

namespace worldisMeTests.Tests
{
    public class AuthBase : BaseTest
    {
        AccountData user = new AccountData(Settings.Email, Settings.Password);
        [SetUp]
        public void SetUpTests()
        {
            man.Navigator.OpenLoginPage();
            man.Login.Login(user);
        }

    }
}

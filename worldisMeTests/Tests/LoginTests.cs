﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Threading;
using worldisMeTests.Poco;

namespace worldisMeTests.Tests
{
    [TestFixture]
    public class LoginTests : BaseTest
    {
        [Test]
        public void LoginWithValidData()
        {
            
            man.Navigator.OpenLoginPage();
            string name = Settings.Email;
            string password = Settings.Password;
            AccountData user = new AccountData(Settings.Email, Settings.Password);
            man.Login.Login(user);

            Assert.IsTrue(man.Login.LoggedIn());
            Thread.Sleep(1000);
        }

        
        [Test]
        public void LoginWithInvalidData()
        {
            man.Navigator.OpenLoginPage();
            AccountData user = new AccountData("user2", "123456");
            man.Login.Login(user);

            Assert.IsTrue(man.Login.AuthError());
            Thread.Sleep(1000);
        }
    }
}

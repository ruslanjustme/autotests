﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System.Collections;

using System.Xml.Serialization;
using System.Collections.Generic;
using System.IO;
using worldisMeTests.Poco;
using System.Reflection;

namespace worldisMeTests.Tests
{
    [TestFixture]
    public class CreatePostTests : AuthBase
    {
        public static IEnumerable<PostData> PostDataFromXml()
        {
            return (List<PostData>)
                new XmlSerializer(typeof(List<PostData>))
                .Deserialize(new StreamReader(
                    Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\posts.xml")
                );
        }

        [Test, TestCaseSource("PostDataFromXml")]
        public void CreatePostTest(PostData post)
        {
            man.Navigator.OpenBlogPage();
            man.Post.CreatePost(post);
            man.Navigator.OpenBlogPage();

            Assert.AreEqual(post.Title + "\r\n" + post.Text, man.Post.GetLastPostTitle());
            man.Login.Logout();
            Thread.Sleep(2000);
        }
    }
}

﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;


namespace worldisMeTests.Tests
{
    [TestFixture]
    public class BaseTest
    {
        protected Manager man;

        [SetUp]
        public void SetupTest()
        {
            man = Manager.GetInstance();
            man.Navigator.OpenHomePage();
        }    
    }
}

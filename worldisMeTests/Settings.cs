﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using static System.Net.Mime.MediaTypeNames;
using System.Reflection;

namespace worldisMeTests
{
    class Settings
    {
        public static string file = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)+@"\settings.xml";
        private static string baseURL;
        private static string email;
        private static string password;
        private static XmlDocument document;

        static Settings()
        {
            if (!System.IO.File.Exists(file))
            {
                throw new Exception("Ex: can't find settings.xml file");
            }
            document = new XmlDocument();
            document.Load(file);
        }

        public static string BaseURL
        {
            get
            {
                if(baseURL==null)
                {
                    XmlNode node = document.DocumentElement.SelectSingleNode("BaseUrl");
                    baseURL = node.InnerText;
                }
                return baseURL;
            }
        }

        public static string Email
        {
            get
            {
                if (email == null)
                {
                    XmlNode node = document.DocumentElement.SelectSingleNode("Login");
                    email = node.InnerText;
                }
                return email;
            }
        }

        public static string Password
        {
            get
            {
                if (password == null)
                {
                    XmlNode node = document.DocumentElement.SelectSingleNode("Password");
                    password = node.InnerText;
                }
                return password;
            }
        }
    }
}
